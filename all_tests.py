import unittest
#import HtmlTestRunner
import first_test
import second_test
import third_test
import xmlrunner


def run_all_test_in_class_generate_html_report():

    # Create a TestSuite object.
    #test_suite = unittest.TestSuite()

    example_tests = unittest.TestLoader().loadTestsFromTestCase(first_test.FirstTest)
    example2_tests = unittest.TestLoader().loadTestsFromTestCase(second_test.SecondTest)
    example3_tests = unittest.TestLoader().loadTestsFromTestCase(third_test.ThirdTest)

    suite = unittest.TestSuite([example_tests, example2_tests, example3_tests])
    # Make all test function 
    #test = unittest.makeSuite(test_case_class)
    #test_suite.addTest(test)

    # Create HtmlTestRunner object and run the test suite.
    #test_runner = HtmlTestRunner.HTMLTestRunner(output='public', combine_reports=True, report_title='Test All', report_name="test-report", add_timestamp=False)
    test_runner = xmlrunner.XMLTestRunner(output='reports')
    test_runner.run(suite)

if __name__=='__main__':
    run_all_test_in_class_generate_html_report()
